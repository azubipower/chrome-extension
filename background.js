/**
 * Created by s.pickels on 13.01.17.
 */
document.addEventListener('DOMContentLoaded', function () {
    var bookmarks = chrome.bookmarks;
    var renderedHTML = "";

    chrome.bookmarks.getChildren('1', function (array) {

        for (var i = 0; i < array.length; i++) {

            if (array[i].url == null) {

                chrome.bookmarks.getChildren(array[i].id, function (arrayTwo) {
                    for (var j = 0; j < arrayTwo.length; j++) {
                        renderedHTML += createElement(arrayTwo[j]);
                    }
                });

            } else {

                renderedHTML += createElement(array[i]);
            }
        }

        setTimeout(function () {
            document.getElementById('js-bookmarks').innerHTML = renderedHTML;
            addEventsToiFrames();
        }, 100);
    });




    function createElement(arrayElement) {

        var icon = 'chrome://favicon/' + arrayElement.url;

        return '<div class="js-wrapper" id="' + arrayElement.id + '"style=" margin:2px; width: 400px; height: 75px; display: inline-block; border: 1px solid #000000; overflow: hidden; text-align: center;">' +
            '<div style="width: 100%; height: 75px; background-color: #134a8b;">' +
            '<a style="text-decoration: none; color: #ababab; font-size: 16px;" href="'+ arrayElement.url +'">' +
            '<img style=" margin-right: 10px; border: 1px solid #ABABAB" alt="favicon"  src="' + icon + '">'+ arrayElement.title +
            '</a>' +
            '</div>' +
            '<iframe style="width: 400px; height: 425px;  display: none;" data-src="' + arrayElement.url + '" src=""></iframe>' +
            '</div>';

    }

    function addEventsToiFrames() {

        var wrapper = document.getElementsByClassName('js-wrapper');

        for (var i = 0; i < wrapper.length; i++) {

            wrapper[i].addEventListener('click', function () {
                if (!(this.classList.contains('opened'))) {
                    this.classList.add('opened');
                    this.style.position = 'relative';
                    this.style.zIndex = '10';
                    this.style.height = '500px';
                    this.firstElementChild.nextElementSibling.style.display = "inline-block";
                    var source = this.firstElementChild.nextElementSibling.dataset.src;
                    this.firstElementChild.nextElementSibling.setAttribute('src', source);
                } else {
                    this.style.position = 'static';
                    this.style.zIndex = 'auto';
                    this.classList.remove('opened');
                    this.style.height = '75px';
                    this.firstElementChild.nextElementSibling.style.display = "none";
                    this.firstElementChild.nextElementSibling.setAttribute('src', '');
                }

            });

        }

    }

});